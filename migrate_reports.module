<?php

/**
 * @file
 * Migrate Reports module.
 */

/**
 * Implements hook_menu().
 */
function migrate_reports_menu() {
  $items = array();

  $items['admin/reports/migrate'] = array(
    'type' => MENU_LOCAL_TASK | MENU_NORMAL_ITEM,
    'title' => 'Migrations',
    'description' => 'Overview of all migrations in the system',
    'page callback' => 'migrate_reports_dashboard',
    'page arguments' => array(),
    'access arguments' => array(MIGRATE_ACCESS_BASIC),
    'access callback' => 'user_access',
  );
  $items['admin/reports/migrate/migrations'] = array(
    'title' => 'All migrations',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  $items['admin/reports/migrate/destinations'] = array(
    'type' => MENU_LOCAL_TASK,
    'title' => 'Destinations',
    'description' => 'Overview of all possible destinations in the system',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('migrate_reports_destinations'),
    'access arguments' => array(MIGRATE_ACCESS_BASIC),
    'access callback' => 'user_access',
    'weight' => 0,
  );

  return $items;
}

function migrate_reports_dashboard() {
  drupal_set_title(t('All migrations'));

  $build = array();

  $build['overview'] = array(
    '#prefix' => '<div>',
    '#markup' => filter_xss_admin(migrate_overview()),
    '#suffix' => '</div>',
  );

  $migrations = migrate_reports_get_migrations();
  $rows = array();
  foreach ($migrations as $migration_record) {
    // Right now, we are just assuming we will get a Migration object.
    /** @var \Migration $migration */
    $migration = Migration::getInstance($migration_record->machine_name);
    $migr_link = l($migration_record->machine_name, sprintf('admin/content/migrate/groups/%s/%s', $migration_record->group_name, $migration_record->machine_name));
    $group = l($migration_record->title, sprintf('admin/content/migrate/groups/%s', $migration_record->group_name));

    $rows[] = array(
      $migr_link,
      $migration_record->class_name,
      $group,
      $migration->getDestination()->__toString(),
      get_class($migration->getSource()),
      (string) $migration->getSource(),
    );
  }

  $build['table'] = array(
    '#theme' => 'table',
    '#header' => array(
      t('Migration name'),
      t('Class name'),
      t('Group'),
      t('Destination'),
      t('Source'),
      t('Query'),
    ),
    '#rows' => $rows,
  );

  return $build;
}

function migrate_reports_destinations() {
  drupal_set_title(t('Destinations'));

  $build = array();

  $build['vtabs'] = array(
    '#type' => 'vertical_tabs',
  );

  // Get all migrations.
  $migration_records = migrate_reports_get_migrations();
  $migrations = array_map(function ($migration_record) {
    return Migration::getInstance($migration_record->machine_name);
  }, $migration_records);

  // TODO: Make this pluggable. See https://www.drupal.org/node/2711011.
  // Handle nodes.
  $node_type_table = array();

  $node_types = node_type_get_types();
  foreach ($node_types as $type => $node_type) {
    $vocab_migrations = array();
    /** @var \Migration $migration */
    foreach ($migrations as $migration) {
      $dest = $migration->getDestination();
      if ($dest instanceof MigrateDestinationNode && $dest->getBundle() == $type) {
        $migr_link = l($migration->getMachineName(), sprintf('admin/content/migrate/groups/%s/%s', $migration->getGroup()->getName(), $migration->getMachineName()));
        $vocab_migrations[] = $migr_link;
      }
    }

    $row = array(
      'class' => array(),
      'data' => array(
        sprintf("%s (%s)", $node_type->name, $type),
        implode(", ", $vocab_migrations),
      ),
    );

    if (!$vocab_migrations) {
      $row['class'][] = 'migrate-error';
    }

    $node_type_table[] = $row;
  }

  $build['node_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node Types'),
    '#group' => 'vtabs',
  );

  $build['node_types']['table'] = array(
    '#theme' => 'table',
    '#header' => array(
      t('Content Type'),
      t('Associated Migrations'),
    ),
    '#rows' => $node_type_table,
  );

  // Handle vocabularies.
  $vocab_table = array();

  $vocabs = taxonomy_vocabulary_get_names();
  foreach ($vocabs as $type => $vocab) {
    $vocab_migrations = array();
    /** @var \Migration $migration */
    foreach ($migrations as $migration) {
      $dest = $migration->getDestination();
      if ($dest instanceof MigrateDestinationTerm && $dest->getBundle() == $type) {
        $migr_link = l($migration->getMachineName(), sprintf('admin/content/migrate/groups/%s/%s', $migration->getGroup()->getName(), $migration->getMachineName()));
        $vocab_migrations[] = $migr_link;
      }
    }

    $row = array(
      'class' => array(),
      'data' => array(
        sprintf("%s (%s)", $vocab->name, $type),
        implode(", ", $vocab_migrations),
      ),
    );

    if (!$vocab_migrations) {
      $row['class'][] = 'migrate-error';
    }

    $vocab_table[] = $row;
  }

  $build['taxonomy_vocabularies'] = array(
    '#type' => 'fieldset',
    '#title' => t('Taxonomy Vocabularies'),
    '#group' => 'vtabs',
  );

  $build['taxonomy_vocabularies']['table'] = array(
    '#theme' => 'table',
    '#header' => array(
      t('Vocabulary'),
      t('Associated Migrations'),
    ),
    '#rows' => $vocab_table,
  );

  return $build;
}

function migrate_reports_get_migrations() {
  $query = db_select('migrate_status', 'ms')
    ->fields('ms');
  $query->leftJoin('migrate_group', 'mg', 'ms.group_name = mg.name');
  $query->fields('mg');
  $query->orderBy('group_name');
  $query->orderBy('machine_name');
  $result = $query->execute()->fetchAllAssoc('machine_name');
  return $result;
}
